package com.qburst.DropWizardAuth.views;

import io.dropwizard.views.View;

public class LoginView extends View {
    private final String username;
    private final String msg;

    public LoginView(String username) {
        super("login.mustache");
        this.username = username;
        this.msg = "";
    }

    public LoginView(String username, String msg) {
        super("login.mustache");
        this.username = username;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public String getUsername() {
        return username;
    }
}
    