package com.qburst.DropWizardAuth.views;

import io.dropwizard.views.View;

public class JoinView extends View {
    private final String username;
    private final String msg;

    public JoinView(String username) {
        super("join.mustache");
        this.username = username;
        this.msg = "";
    }

    public JoinView(String username, String msg) {
        super("join.mustache");
        this.username = username;
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    public String getUsername() {
        return username;
    }
}
    