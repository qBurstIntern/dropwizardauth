package com.qburst.DropWizardAuth.views;

import com.qburst.DropWizardAuth.modals.PersonModal;
import io.dropwizard.views.View;

public class PersonView extends View {
	private PersonModal person;

    public PersonView(PersonModal person) {
        super("user.mustache");
        this.person = person;
    }

    public PersonModal getPerson() {
        return person;
    }
}
    