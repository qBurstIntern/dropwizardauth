package com.qburst.DropWizardAuth.utils;

import io.dropwizard.jersey.sessions.Session;

import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

public class RoutingUtils {
	public static Response redirectToURI(String newURI) {
		URI uri = null;
		try {
			uri = new URI(newURI);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return Response.seeOther(uri).build();
	}

	public static Boolean authenticate(@Session HttpSession session) {
		String sessionAttribute = ((String) session.getAttribute("username"));
		if (sessionAttribute == null || sessionAttribute.isEmpty()) {
			return false;
		}
		return true;
	}
}