package com.qburst.DropWizardAuth.utils;

import com.qburst.DropWizardAuth.views.ErrorView;
import org.apache.log4j.Logger;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class RuntimeExceptionMapper implements ExceptionMapper<RuntimeException> {
	final static Logger logger = Logger.getLogger(RuntimeExceptionMapper.class);

	public Response toResponse(RuntimeException exception) {
		logger.error(exception.getMessage());
		Response response = Response
			.serverError()
			.entity(new ErrorView(exception))
			.build();
		return response;
	}
}