package com.qburst.DropWizardAuth.configuration;

public class CassandraFactory {
    private String host;
    private String keyspace;

    public void setHost(String host) {
        this.host = host;
    }

    public void setKeyspace(String keyspace) {
        this.keyspace = keyspace;
    }

    public String getHost() {
        return host;
    }

    public String getKeyspace() {
        return keyspace;
    }
}
