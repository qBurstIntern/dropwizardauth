package com.qburst.DropWizardAuth.configuration;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class DropWizardAuthConfiguration extends Configuration {
	@Valid
	@NotNull
	@JsonProperty("cassandra")
    private CassandraFactory cassandra = new CassandraFactory();

    public CassandraFactory getCassandra() {
        return cassandra;
    }
}