package com.qburst.DropWizardAuth.application;

import com.qburst.DropWizardAuth.configuration.DropWizardAuthConfiguration;
import com.qburst.DropWizardAuth.db.PersonDAO;
import com.qburst.DropWizardAuth.resources.ApiResource;
import com.qburst.DropWizardAuth.resources.PersonResource;
import com.qburst.DropWizardAuth.utils.RuntimeExceptionMapper;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import org.eclipse.jetty.server.session.SessionHandler;

public class DropWizardAuthApplication extends Application<DropWizardAuthConfiguration> {
    public static void main(String[] args) throws Exception {
        new DropWizardAuthApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<DropWizardAuthConfiguration> bootstrap) {
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                )
        );

        bootstrap.addBundle(new ViewBundle<DropWizardAuthConfiguration>());
        bootstrap.addBundle(new AssetsBundle("/assets", "/public", "favicon.ico"));
        bootstrap.addBundle(new AssetsBundle("/favicon.ico", "/public/favicon.ico", "favicon.ico", "favicon"));
    }

    @Override
    public void run(DropWizardAuthConfiguration configuration, Environment environment) {
        final PersonDAO dao = new PersonDAO(configuration);

        final PersonResource resource = new PersonResource();
        final ApiResource api = new ApiResource(dao);
        environment.servlets().setSessionHandler(new SessionHandler());
        environment.jersey().register(resource);
        environment.jersey().register(api);
        environment.jersey().register(new RuntimeExceptionMapper());
    }
}