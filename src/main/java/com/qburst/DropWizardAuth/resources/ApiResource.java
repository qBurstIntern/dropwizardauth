package com.qburst.DropWizardAuth.resources;

import com.qburst.DropWizardAuth.db.PersonDAO;
import com.qburst.DropWizardAuth.modals.PersonModal;
import com.qburst.DropWizardAuth.utils.HashingUtils;
import io.dropwizard.jersey.sessions.Session;

import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Pattern;

@Path("/api")
public class ApiResource {
    private PersonDAO personDAO;

    public ApiResource(PersonDAO personDAO) {
        this.personDAO = personDAO;
    }

    @Path("/logout")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public Boolean logout(@Session HttpSession session) {
        session.setAttribute("username", "");
        session.setAttribute("id", "");
        session.invalidate();
        return true;
    }

    @Path("/login")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String postLogin(@FormParam("username") String username, @FormParam("password") String password, @Session HttpSession session) {
        if (username.trim().isEmpty() || password.trim().isEmpty()) {
            return "Please fill all the fields.";
        }

        ArrayList<PersonModal> listLogin = personDAO.login(username, HashingUtils.GenerateHash(password));
        if (!listLogin.isEmpty()) {
            session.setAttribute("username", username);
            session.setAttribute("id", listLogin.get(0).getId());
            return "true";
        }

        return "Please enter a valid username and password.";
    }

    @Path("/join")
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    public String postJoin(@FormParam("username") String username, @FormParam("password") String password, @FormParam("confirm") String confirm) {
        if (username.trim().isEmpty() || password.trim().isEmpty() || confirm.trim().isEmpty()) {
            return "Please fill all the fields.";
        }

        if (username.length() > 30) {
            return "Username must NOT be longer that 30 characters.";
        }

        Pattern p = Pattern.compile("[^a-zA-Z]");
        if (username.contains(" ") || p.matcher(username).find()) {
            return "Username must NOT have space or special characters.";
        }

        if (password.length() < 4) {
            return "Password must be atleast 4 characters.";
        }

        if (!password.equals(confirm)) {
            return "Passwords don't match.";
        }

        ArrayList<PersonModal> listDuplicate = personDAO.findDuplicate(username);
        if (!listDuplicate.isEmpty()) {
            return "This username already exists.";
        }

        final PersonModal newPerson = new PersonModal(UUID.randomUUID().toString(), username, HashingUtils.GenerateHash(password));
        if (personDAO.insert(newPerson)) {
            return "true";
        } else {
            return "Something went wrong at server. Please try again.";
        }
    }
}
