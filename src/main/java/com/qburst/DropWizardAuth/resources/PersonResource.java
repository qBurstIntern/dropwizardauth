package com.qburst.DropWizardAuth.resources;

import com.qburst.DropWizardAuth.modals.PersonModal;
import com.qburst.DropWizardAuth.utils.RoutingUtils;
import com.qburst.DropWizardAuth.views.JoinView;
import com.qburst.DropWizardAuth.views.LoginView;
import com.qburst.DropWizardAuth.views.PersonView;
import io.dropwizard.jersey.sessions.Session;

import javax.servlet.http.HttpSession;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.UUID;

@Path("/")
public class PersonResource {
    @GET
    @Produces(MediaType.TEXT_HTML)
    public Object getPerson(@Session HttpSession session) {
        if (RoutingUtils.authenticate(session)) {
            return new PersonView(new PersonModal((String) session.getAttribute("id"), (String) session.getAttribute("username"), ""));
        } else {
            return RoutingUtils.redirectToURI("/login");
        }
    }

    @Path("/login")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public LoginView getLogin() {
        return new LoginView("");
    }

    @Path("/join")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public JoinView getJoin() {
        return new JoinView("");
    }
}
