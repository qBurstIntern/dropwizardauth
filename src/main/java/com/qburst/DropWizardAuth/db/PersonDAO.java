package com.qburst.DropWizardAuth.db;

import com.datastax.driver.core.*;
import com.qburst.DropWizardAuth.configuration.DropWizardAuthConfiguration;
import com.qburst.DropWizardAuth.modals.PersonModal;
import com.qburst.DropWizardAuth.modals.mappers.PersonMapper;

import java.util.ArrayList;

public class PersonDAO {
	Cluster cluster;
	Session session;

	public PersonDAO(DropWizardAuthConfiguration configuration) {
		this.cluster = Cluster.builder().addContactPoint(configuration.getCassandra().getHost()).build();
		this.session = this.cluster.connect(configuration.getCassandra().getKeyspace());
	}

	protected void finalize() {
		cluster.close();
	}

	public ArrayList<PersonModal> login(String username, String password) {
		PreparedStatement statement = session.prepare("SELECT id from people where username = :username AND password = :password ALLOW FILTERING;");
		BoundStatement boundStatement = new BoundStatement(statement);
		return PersonMapper.map(session.execute(boundStatement.bind(username, password)));
	}

	public ArrayList<PersonModal> findDuplicate(String username) {
		PreparedStatement statement = session.prepare("SELECT id FROM people WHERE username = ?;");
		BoundStatement boundStatement = new BoundStatement(statement);
		return PersonMapper.map(session.execute(boundStatement.bind(username)));
	}

	public Boolean insert(PersonModal person) {
		PreparedStatement statement = session.prepare("INSERT INTO people(id, username, password) VALUES (?, ?, ?);");
		BoundStatement boundStatement = new BoundStatement(statement);
		return session.execute(boundStatement.bind(person.getId(), person.getUsername(), person.getPassword())).wasApplied();
	}
}