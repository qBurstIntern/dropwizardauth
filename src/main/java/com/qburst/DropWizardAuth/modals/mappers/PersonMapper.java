package com.qburst.DropWizardAuth.modals.mappers;

import com.datastax.driver.core.ColumnDefinitions;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.qburst.DropWizardAuth.modals.PersonModal;
import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;


public class PersonMapper {
	public static ArrayList<PersonModal> map(ResultSet results) {
		ArrayList<PersonModal> list = new ArrayList<PersonModal>();
		List<ColumnDefinitions.Definition> columns = results.getColumnDefinitions().asList();

		for (Row row : results) {
			PersonModal person = new PersonModal();
			for (ColumnDefinitions.Definition col : columns) {
				try {
					BeanUtils.setProperty(person, col.getName(), row.getString(col.getName()));
				} catch (InvocationTargetException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			}
			list.add(person);
		}

		return list;
	}
}